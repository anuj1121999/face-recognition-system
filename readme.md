A Face Recognition and Verification System is build by implementing Siamese Network (One-Shot Learning) hence very fast.
Here a pre-trained CNN is used which is trained on the triplet loss function. 
The CNN outputs a 128 vector encoding.These encodings are used to compare different images.
The project was build as a part of specialization deeplearning.ai.
Implemented in python along with TensorFlow and Keras.